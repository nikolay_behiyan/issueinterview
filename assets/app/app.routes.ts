import {provideRouter, RouterConfig} from '@angular/router';
import {PageNotFoundComponent} from './pageNotFound/pageNotFound.component';
import {postRoutes} from './post/post.routes';
import {PostComponent} from "./post/post.component";
import {PostListComponent} from "./post/postList.component";

const routes:RouterConfig = <RouterConfig>[
    {path: '', component: PostListComponent},
    ...postRoutes,
    {path: '**', component: PageNotFoundComponent}
];

export const appRouterProvider = [
  provideRouter(routes)
];



/// <reference path="../../index.d.ts" />

import {bootstrap} from "@angular/platform-browser-dynamic";
import {AppComponent} from "./AppComponent";
import {appRouterProvider} from './app.routes';
import {HTTP_PROVIDERS} from '@angular/http';

bootstrap(AppComponent,[appRouterProvider, HTTP_PROVIDERS]);
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IComment} from "./comment.interface";
import {IUser} from "../user/user.interface";
import {CommentService} from "./comment.service";
import {CommentNestedComponent} from "./commentNested.component";


@Component({
    selector: 'app-comment',
    templateUrl: './app/comment/comment.template.html',
    providers: [CommentService],
    directives: [CommentNestedComponent]
})

export class CommentComponent implements OnInit{
    private user:IUser = {
        username: '',
        userId: 0,
        userImg:''
    }
    private comment: IComment = {
        postId: 0,
        commentId: 0,
        commentDate: new Date(),
        commentOwner: this.user,
        commentText: '',
        commentedId: 0,
        likes: 0};
    private commentList = [];
    commentDestination = {
        username: '',
        destination : undefined
    }
    messageText = '';
    postId: number;
    update: boolean = false;

    constructor(private commentService: CommentService,
                private activatedRoute: ActivatedRoute){}

    ngOnInit(){
        this.activatedRoute.params.subscribe((params:{})=>{
            this.postId = params['id'];
            console.log(this.postId);
            this.getComments(this.postId);
        })

    }
    getComments(postId: number){
        this.commentService.getCommentList(postId).then(((commentList: [IComment]) => {
            this.commentList = commentList;
        }))
    }
    scrollBottom(comment: any){
        window.scrollTo(0, document.body.scrollHeight);
        this.commentDestination.username = comment.commentOwner.username;
        this.commentDestination.destination = comment;

    }
    addComment(){
        this.commentService.addComment(this.messageText, this.commentDestination, this.postId);
        this.update = true;
        this.ngOnInit();

        this.messageText = '';
    }
    removeDestination(){
        this.messageText = '';
        this.commentDestination.destination = undefined;
        this.commentDestination.username = '';
    }
    done(event){
        setTimeout(()=>{
            this.update = false;
        }, 0)

    }
    addLikes(comment){
        comment.likes++; this.ngOnInit();
    }

}
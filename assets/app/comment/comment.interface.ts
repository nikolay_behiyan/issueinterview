import {IUser} from "../user/user.interface";

export interface IComment{
    postId: number,
    commentId: number,
    commentDate: Date,
    commentOwner: IUser,
    commentText: string,
    commentedId: number,
    likes: number
}
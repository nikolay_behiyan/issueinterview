import {Injectable} from '@angular/core';
import {IComment} from "./comment.interface";
import {IUser} from "../user/user.interface";


@Injectable()


export class CommentService{

    private commentList = [
        {postId: 1, commentId: 1, commentDate: new Date(2016, 2, 13), commentOwner: {username: 'John Doe', userId: 1, userImg: '/images/9.png'}, commentText: 'Прикольно! Я могу комметировать посты', commentedId: null, likes: 2},
        {postId: 1, commentId: 2, commentDate:  new Date(2016, 2, 13), commentOwner: {username: 'Jack Spancer', userId: 2, userImg: '/images/6.gif'}, commentText: 'А я могу коментить твои коменты', commentedId: 1, likes: 4},
        {postId: 2, commentId: 3, commentDate:  new Date(2016, 2, 13), commentOwner: {username: 'Tim Doe', userId: 3, userImg: '/images/7.jpg'}, commentText: 'Классный пост', commentedId: null, likes: 2},
        {postId: 3, commentId: 4, commentDate:  new Date(2016, 2, 13), commentOwner: {username: 'Mikky Maccain', userId: 4, userImg: '/images/8.jpg'}, commentText: 'Awesome i can write comments here', commentedId: null, likes: 0},
        {postId: 1, commentId: 5, commentDate:  new Date(2016, 2, 13), commentOwner: {username: 'Mike Tyson', userId: 5, userImg: '/images/9.png'}, commentText: 'А мне не понравился', commentedId: null, likes: 1},
        {postId: 1, commentId: 6, commentDate:  new Date(2016, 2,13), commentOwner: {username: 'John Doe', userId: 1, userImg: '/images/9.png'}, commentText: 'Вснем привет! давайте пообсуждаем', commentedId: null, likes: 5}

        ]

    constructor(){}

    getCommentList( postId: number){
        return new Promise(resolve => {
            resolve(this.commentList.filter(comment => {
                return comment.postId == postId && comment.commentedId == null;
            }))
        });
    }
    getNestedComments(commentedId: number){
        return new Promise(resolve => {
            resolve(this.commentList.filter(comment => {
                return comment.commentedId == commentedId;
            }));
        });
    }
    addComment(messageText: string , destination: any, postId){
        console.log(messageText);
        if(messageText.length > 0){
            var newId = this.commentList[this.commentList.length -1].commentId + 1;
            var newComment = {
                postId: postId ,
                commentId: newId,
                commentDate: new Date(),
                commentOwner: {username: 'Не известный пользователь', userId: 10, userImg: '/images/5.png'},
                commentText: messageText,
                commentedId: destination.destination ? destination.destination.commentId : null,
                likes: 0}
            this.commentList.push(newComment);
            console.log(this.commentList);
        }

    }
}
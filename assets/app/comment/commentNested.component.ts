import {Component, OnInit, EventEmitter} from '@angular/core';
import {CommentService} from "./comment.service";
import {IComment} from "./comment.interface";


@Component({
    selector: 'app-commentNested',
    templateUrl: './app/comment/commentNested.template.html',
    inputs: ['nestedComment', 'update'],
    outputs: ['done']
})

export class CommentNestedComponent implements OnInit{
    nestedComment: number;
    private nestedComments = [];
    constructor(private commentService: CommentService){}
    done = new EventEmitter();

    ngOnInit(){
        this.commentService.getNestedComments(this.nestedComment).then((comments: [IComment]) => {
            this.nestedComments = comments;
        })
    }

    set update(value){
        if(value){
            this.ngOnInit();
            this.done.emit({update: false});
        }
    }

}
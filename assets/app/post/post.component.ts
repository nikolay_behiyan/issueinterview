import {Component, OnInit} from '@angular/core';
import {PostService} from "./post.service";
import {ActivatedRoute, Router,  ROUTER_DIRECTIVES} from "@angular/router";
import {IPost} from "./post.interface";
import {CommentComponent} from "../comment/comment.component";

@Component({
    selector: 'app-post',
    templateUrl: './app/post/post.template.html',
    providers: [PostService],
    directives: [ROUTER_DIRECTIVES, CommentComponent]
})

export class PostComponent  implements OnInit{
    private postItem: IPost = {
        postName: '',
        postId: 0,
        postTime: new Date(),
        imgSrc: '',
        postText: ''
    };
    private postList = [];

    constructor(private postService: PostService,
                private activatedRoute: ActivatedRoute ){}
    ngOnInit(){
        this.activatedRoute.params.subscribe((params:{})=>{
            let postId = params['id'];
            this.postService.getPost(postId).then((postItem: IPost)=>{
                this.postItem = postItem;
            })
        })
        this.postService.getPostList().then((postList: [IPost]) =>{
            this.postList = postList;
        })
    }



}
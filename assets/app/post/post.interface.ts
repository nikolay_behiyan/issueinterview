import {IComment} from '../comment/comment.interface';

export interface IPost{
    postName: string,
    postId: number,
    postTime: Date,
    imgSrc: string,
    postText: string
}
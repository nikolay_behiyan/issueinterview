import {RouterConfig} from '@angular/router';
import {PostComponent} from "./post.component";


export const postRoutes: RouterConfig = <RouterConfig>[

    {path: 'post/:id', component: PostComponent}
]
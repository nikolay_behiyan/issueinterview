import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PostService} from "./post.service";
import {IPost} from "./post.interface";


@Component({
    selector: 'app-postList',
    templateUrl: './app/post/postList.template.html',
    providers: [PostService]
})

export class PostListComponent implements OnInit{
    private postList = [];
    constructor(private postService: PostService,
                private router: Router){}


    ngOnInit(){
        this.postService.getPostList().then((postList: [IPost]) =>{
            this.postList = postList;
        })
    }
    gotoPost(postId: string){
        this.router.navigate(['/post', postId]);
    }
}
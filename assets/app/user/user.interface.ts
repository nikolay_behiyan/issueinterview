export interface IUser{
    username: string,
    userId: number,
    userImg: string
}
/**
 * Created by Nikolay on 7/4/2016.
 */

var gulp = require("gulp");
var typescript = require("gulp-typescript");
var sass = require("gulp-sass");

var lib = "public/lib"
var typescriptProject = typescript.createProject("tsconfig.json");

gulp.task("build-ts", function(){
    return typescriptProject.src()
        .pipe(typescript(typescriptProject))
        .js.pipe(gulp.dest("public/app/"));
});
gulp.task("build-sass", function(){
    return gulp.src("./assets/sass/**/*.sass")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/stylesheets/'));
});
gulp.task("build-copy", function(){
   return gulp.src(["./assets/**/*.html", "./assets/templates/**/*.htm","./assets/**/*.css"])
    .pipe(gulp.dest('./public'));
});
gulp.task('build-lib', function(){
    // Angular 2 Framework
    gulp.src('node_modules/@angular/**')
        .pipe(gulp.dest(lib + '/@angular'));

    //es6-shim
    gulp.src('node_modules/es6-shim/**')
        .pipe(gulp.dest(lib + '/es6-shim/'));

    //reflect metadata
    gulp.src('node_modules/reflect-metadata/**')
        .pipe(gulp.dest(lib + '/reflect-metadata/'));

    //rxjs
    gulp.src('node_modules/rxjs/**')
        .pipe(gulp.dest(lib + '/rxjs/'));

    //systemjs
    gulp.src('node_modules/systemjs/**')
        .pipe(gulp.dest(lib + '/systemjs/'));

    //zonejs
    return gulp.src('node_modules/zone.js/**')
        .pipe(gulp.dest(lib + '/zone.js/'));
})
gulp.task("watch-ts", function(){
    gulp.watch('./assets/app/**/*.ts', ["build-ts"])
});
gulp.task("watch-sass", function(){
    gulp.watch("./assets/sass/**/*.sass", ['build-sass']);
});
gulp.task("watch-all", function(){
    gulp.watch("./assets/**/*.{html,css,htm}", ['build-copy']);
});
gulp.task("watch", ["watch-ts","watch-sass"]);

gulp.task('default',["build-ts","build-copy","build-sass", "watch"]);

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { });
});
router.all('*', function(req,res){
    res.status(200);
    res.render('index', {});
});

module.exports = router;
